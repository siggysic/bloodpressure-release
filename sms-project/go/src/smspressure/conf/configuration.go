package conf

import (
	"bitbucket.org/dotography-code/luciano/configure"
)

var Configuration *configure.Config

func Load() {
	Configuration = configure.New()
	Configuration.Add("development", "conf/application.json")
	Configuration.Add("production", "conf/production.json")
}
