package assets

import (
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func render(path string, ct string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		dir, _ := os.Getwd()
		txt, _ := ioutil.ReadFile(dir + "/view/" + path)
		w.Header().Add("Content-Type", ct)
		w.Write(txt)
	}
}

func Public(path string) func(http.ResponseWriter, *http.Request) {
	if strings.HasSuffix(path, ".css") {
		return render(path, "text/css")
	}
	return render(path, "text/plain")
}

func Files() []string {
	var files []string
	dir, _ := os.Getwd()
	err := filepath.Walk(dir+"/view", func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	orSurfix := func(data string, vs []string) bool {
		for _, v := range vs {
			if strings.HasSuffix(data, v) {
				return true
			}
		}
		return false
	}
	paths := []string{}
	for _, file := range files {
		if orSurfix(file, []string{".css", ".js", ".map", ".png", ".jpg", ".ttf", ".ico", ".txt", ".scss", ".less", ".woff2", ".woff", ".ttf", ".svg", ".eot", ".otf"}) {
			path := strings.Replace(file, dir, "", 1)
			path = strings.Replace(path, "/view", "", 1)
			path = strings.Replace(path, "\\view", "", 1)
			path = strings.Replace(path, "\\", "/", -1)
			paths = append(paths, path)
		}
	}
	return paths
}
