package sms

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"smspressure/conf"
	"smspressure/model"
	"smspressure/repository"
	"smspressure/util"
	"smspressure/view"
	"strconv"
	"strings"

	"github.com/gorilla/schema"
	"github.com/qiniu/iconv"

	"bitbucket.org/dotography-code/luciano/logger/stdout"
)

const TxtSMS = "sms"

var decoder = schema.NewDecoder()

func charset(str string) string {
	cd, err := iconv.Open("tis-620", "utf-8")
	if err != nil {
		stdout.Error("iconv.Open failed!")
		return str
	}
	defer cd.Close()
	return cd.ConvString(str)
	return str
}

func template(msms model.SMS) string {
	bpMessage := "ค่าผิดพลาด"
	bpSuggest := "ค่าผิดพลาด"
	afib := "ค่าผิดพลาด"

	iSys, _ := strconv.Atoi(msms.Sys)
	iDia, _ := strconv.Atoi(msms.Dia)

	if msms.Afib == "0" {
		afib = "ไม่มี"
	} else if msms.Afib == "1" {
		afib = "มี"
	}

	if iSys < iDia {
		switch {
		case 60 <= iDia && iDia <= 80:
			bpMessage = "ปกติ"
			bpSuggest = "ตรวจเช็คความดันปกติ"
		case 80 < iDia && iDia <= 85:
			bpMessage = "ค่อนข้างสูง"
			bpSuggest = "ตรวจเช็คความดันอย่างสม่ำเสมอ"
		case 85 < iDia && iDia <= 100:
			bpMessage = "สูง"
			bpSuggest = "พบแพทย์"
		case iDia > 100:
			bpMessage = "สูงมาก"
			bpSuggest = "พบแพทย์ทันที"
		default:
			bpMessage = "ค่าผิดพลาด"
			bpSuggest = "ค่าผิดพลาด"
		}
	} else {
		switch {
		case 100 <= iSys && iSys <= 130:
			bpMessage = "ปกติ"
			bpSuggest = "ตรวจเช็คความดันปกติ"
		case 130 < iSys && iSys <= 135:
			bpMessage = "ค่อนข้างสูง"
			bpSuggest = "ตรวจเช็คความดันอย่างสม่ำเสมอ"
		case 135 < iSys && iSys <= 160:
			bpMessage = "สูง"
			bpSuggest = "พบแพทย์"
		case iSys > 160:
			bpMessage = "สูงมาก"
			bpSuggest = "พบแพทย์ทันที"
		default:
			bpMessage = "ค่าผิดพลาด"
			bpSuggest = "ค่าผิดพลาด"
		}
	}

	data := "ค่าความดันโลหิต (ตัวบน) sys: " + msms.Sys + " mmHg%0aค่าความดันโลหิต (ตัวล่าง) dia: " + msms.Dia + " mmHg%0aค่าอัตราการเต้นของหัวใจ: " + msms.Hb + " P/min%0aภาวะหัวใจสั่นพริ้ว Afib: " + afib
	t := "ระดับความดันโลหิตของคุณอยู่ในระดับ: " + bpMessage + "\nคำแนะนำ: " + bpSuggest + "\n\n" + data + "\n\nข้อมูลเพิ่มเติม\nwww.samh.co.th/microlife/afib"
	t = strings.Replace(t, "+", " ", -1)
	t = strings.Replace(t, "%28", "(", -1)
	t = strings.Replace(t, "%29", ")", -1)
	t = strings.Replace(t, "%C5", "/", -1)
	t = strings.Replace(t, "%5C", "\\", -1)
	t = strings.Replace(t, "\\n", "%0a", -1)
	return t
}

func sendSMS(msms model.SMS) error {
	url := conf.Configuration.GetString("sms.url")
	username := conf.Configuration.GetString("sms.credentials.username")
	password := conf.Configuration.GetString("sms.credentials.password")
	sendername := conf.Configuration.GetString("sms.sender_name")
	params := fmt.Sprintf("User=%s&Password=%s&Msnlist=%s&Msg=%s&Sender=%s", username, password, msms.Phone, template(msms), sendername)
	payload := strings.NewReader(charset(params))
	req, _ := http.NewRequest(http.MethodPost, url, payload)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	if !strings.HasPrefix(string(body), "Status=0,") {
		return errors.New(string(body))
	}
	stdout.Info("sending sms success")
	return nil
}

func Submit(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	// Read request body
	bytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		stdout.Error("read request body error occur", err.Error())
		view.Renderer.HTML(w, http.StatusBadRequest, "from", map[string]string{"alertMsg": "request data is invalid"})
		return
	}

	// Parsing body to model struct
	now := util.TimeNow()
	msms := model.SMS{ID: util.GenerateID(TxtSMS), CreatedAt: now, UpdatedAt: now}
	body := string(bytes)
	if strings.HasPrefix(body, "{") && strings.HasSuffix(body, "}") {
		json.Unmarshal(bytes, &msms)
	} else {
		q, e := url.ParseQuery(body)
		if e != nil {
			view.Renderer.HTML(w, http.StatusBadRequest, "form", map[string]string{"alertMsg": "request data is invalid"})
			return
		}
		decoder.Decode(&msms, q)
	}

	// Validate
	if !msms.Valid() {
		stdout.Error("invalid request body,", body)
		view.Renderer.HTML(w, http.StatusBadRequest, "form", map[string]string{"alertMsg": "request data is invalid"})
		return
	}

	// Store into database
	go func() {
		defer func() { recover() }()
		err = repository.SMS().Insert(msms)
		if err != nil {
			stdout.Error("store data into the database failure occur", err.Error())
			return
		}
		stdout.Info("store data into the database successfully")
	}()

	// Send SMS to Client
	if err := sendSMS(msms); err != nil {
		stdout.Error("send message error occur", err.Error())
		view.Renderer.HTML(w, http.StatusBadRequest, "from", map[string]string{"alertMsg": "request data is invalid"})
		return
	}

	// Response to caller
	view.Renderer.HTML(w, http.StatusOK, "form", map[string]string{"alertMsg": "send data successfully"})
}

func Form(w http.ResponseWriter, r *http.Request) {
	urlQuery := r.URL.Query()
	sys := strings.TrimSpace(urlQuery.Get("sys"))
	dia := strings.TrimSpace(urlQuery.Get("dia"))
	hb := strings.TrimSpace(urlQuery.Get("hb"))
	afib := strings.TrimSpace(urlQuery.Get("afib"))

	if len(sys) == 0 || len(dia) == 0 || len(hb) == 0 || len(afib) == 0 {
		view.Renderer.HTML(w, http.StatusBadRequest, "form", map[string]string{"alertMsg": "The pressure result not found"})
		return
	}
	view.Renderer.HTML(w, http.StatusOK, "form", map[string]string{"sys": sys, "dia": dia, "hb": hb, "afib": afib})
}
