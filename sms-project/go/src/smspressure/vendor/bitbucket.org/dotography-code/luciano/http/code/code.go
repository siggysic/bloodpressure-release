package code

const (
	OK                  = 200
	Created             = 201
	BadRequest          = 400
	Unauthorized        = 401
	Gone                = 410
	NotFound            = 404
	InternalServerError = 500
	ServiceUnavailable  = 503
)
