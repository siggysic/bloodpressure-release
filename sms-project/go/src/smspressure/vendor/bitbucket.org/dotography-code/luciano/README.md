## Luciano ##

Golang library make for reduce time and cost in development the Micro services by Golang

### What does supported library now? ###
- Memory cache [example](./example/cache.md)
- HTTP caller [example](./example/http.md)
- Standard Output Logging [example](./example/stdout.md)
- MongoDB [example](./example/mongodb.md)
- Standard Response Template [example](./example/response.md)
- JWT [example](./example/jwt.md)
- Kafka Producer [example](./example/kafka-producer.md)
- Kafka Consumer [example](./example/kafka-consumer.md)

### How to install this library into your project? ###
run this command on your terminal
```go
  go get bitbucket.org/dotography-code/luciano
```

if you need to update this library. you just to run this command.
```go
  go get -u bitbucket.org/dotography-code/luciano
```
