package rule

import (
	"encoding/json"
	"fmt"
)

func AnyExistIn(ls []interface{}) Rule {
	return func(key string, value interface{}) *Failure {
		if !isValidRules(key, value)(Required(), IsString()) {
			return nil
		}
		for _, v := range ls {
			if value == v {
				return nil
			}
		}
		bytes, _ := json.Marshal(ls)
		return error(key, fmt.Sprintf("does not contain %s", string(bytes)))
	}
}
