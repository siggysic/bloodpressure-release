package model

import (
	"strings"
	"time"
)

type SMS struct {
	ID        string    `json:"id" bson:"id" schema:"id"`
	Name      string    `json:"name" bson:"name" schema:"name"`
	Phone     string    `json:"phone" bson:"phone" schema:"phone"`
	Sys       string    `json:"sys" bson:"sys" schema:"sys"`
	Dia       string    `json:"dia" bson:"dia" schema:"dia"`
	Hb        string    `json:"hb" bson:"hb" schema:"hb"`
	Afib      string    `json:"afib" bson:"afib" schema:"afib"`
	CreatedAt time.Time `json:"created_at" bson:"created_at" schema:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at" schema:"updated_at"`
}

func (p SMS) Valid() bool {
	return len(strings.TrimSpace(p.ID)) != 0 &&
		len(strings.TrimSpace(p.Phone)) != 0 &&
		len(strings.TrimSpace(p.Sys)) != 0 &&
		len(strings.TrimSpace(p.Dia)) != 0 &&
		len(strings.TrimSpace(p.Hb)) != 0 &&
		len(strings.TrimSpace(p.Afib)) != 0
}
