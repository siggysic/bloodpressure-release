package repository

import (
	"smspressure/conf"

	"bitbucket.org/dotography-code/luciano/mongodb"
	"github.com/globalsign/mgo"
)

func connect(collection string) *mgo.Collection {
	url := conf.Configuration.GetString("repository.mongo.url")
	database := conf.Configuration.GetString("repository.mongo.database")
	option := mongodb.Option{
		Username: conf.Configuration.GetString("repository.mongo.credentials.username"),
		Password: conf.Configuration.GetString("repository.mongo.credentials.password"),
	}
	return mongodb.NewWithOption(url, database, option).Database.C(collection)
}

func SMS() *mgo.Collection {
	collection := conf.Configuration.GetString("repository.mongo.collections.sms")
	return connect(collection)
}

func Pressure() *mgo.Collection {
	collection := conf.Configuration.GetString("repository.mongo.collections.pressure")
	return connect(collection)
}
