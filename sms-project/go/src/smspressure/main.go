package main

import (
	"fmt"
	"log"
	"net/http"

	"smspressure/assets"
	"smspressure/conf"
	"smspressure/service/sms"
	"smspressure/view"

	"bitbucket.org/dotography-code/luciano/logger/stdout"

	"github.com/gorilla/mux"
	"github.com/thedevsaddam/renderer"
)

func init() {
	// Load configuration
	conf.Load()

	// Load views
	view.Renderer = renderer.New(renderer.Options{ParseGlobPattern: "./view/*.html"})
}

func main() {
	port := conf.Configuration.GetInt("port")
	stdout.Info("service starting on port", port)
	router := mux.NewRouter()
	router.HandleFunc("/", sms.Form).Methods(http.MethodGet)
	router.HandleFunc("/", sms.Submit).Methods(http.MethodPost)
	for _, path := range assets.Files() {
		router.HandleFunc(path, assets.Public(path)).Methods(http.MethodGet)
	}
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), router))
}
