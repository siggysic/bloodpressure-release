package util

import "time"

func TimeNow() time.Time {
	return time.Now()
}
