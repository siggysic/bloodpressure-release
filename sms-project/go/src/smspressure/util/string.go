package util

import (
	"fmt"

	"github.com/google/uuid"
)

func GenerateID(prefix string) string {
	return fmt.Sprintf("%v:%v", prefix, uuid.New().String())
}
