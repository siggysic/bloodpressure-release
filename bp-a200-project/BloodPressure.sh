#! /bin/sh

if [ "${ACTION}" = add -a -d "/sys${DEVPATH}" ]; then

	echo "add ${DEVPATH}" >>/tmp/trigger.log

	sleep 10

	chown root /dev/bpddl

	chmod 777 /dev/bpddl

	stty -F /dev/bpddl -a

	mkdir -p /var/lock/lockdev	

	chmod 777 /var/lock/lockdev

	mkdir -p "/BloodPressure/bpddl_report" && cd "/BloodPressure/bpddl_report"

	bpddl -F bpddl.log  -f

fi
