#!/bin/bash

source starter.conf

location=$PWD

if [ -f "$PWD/bpddl-program.tar.gz" ]; then
	rm -rf /etc/bpddl.conf
	tar zxvf "$PWD/bpddl-program.tar.gz"
	cd "$PWD/bpddl-program"
	make
	make install
	cd ..
	rm -rf "$PWD/bpddl-program"
	rm -rf "$PWD/bpddl-program.tar.gz"
	cd
	echo "Installed bpddl program.."
else
	echo "Not required install"
fi

vendor=$vendor_id

device=$device_id

folder=$destination_folder_name

echo "Load starter.conf"

rm -rf /etc/udev/rules.d/TriggerBloodPressure.rules
rm -rf "$folder/BloodPressure.sh"
rm -rf "$folder/sms-trigger.sh"
rm -rf "$folder/starter.conf"

echo "Reset files"

mkdir -p "$folder"

echo SUBSYSTEM==\"tty\", ATTRS{idVendor}==\"$vendor\", ATTRS{idProduct}==\"$device\", RUN+=\"$folder/BloodPressure.sh\", SYMLINK+=\"bpddl\" >> /etc/udev/rules.d/TriggerBloodPressure.rules

chmod 777 /etc/udev/rules.d/TriggerBloodPressure.rules

/etc/init.d/udev restart

echo "Created trigger usb file.."

cp "$location/sms-trigger.sh" "$folder/sms-trigger.sh"
cp "$location/BloodPressure.sh" "$folder/BloodPressure.sh"
cp "$location/starter.conf" "$folder/starter.conf"

chmod 777 "$folder/BloodPressure.sh"
chmod 777 "$folder/sms-trigger.sh"
chmod 777 "$folder/starter.conf"

echo "Created run file.."
