#!/bin/bash

FILEPATH="/BloodPressure/bpddl_report"
URL="http://localhost:8080"

cd "$FILEPATH"
PREVIOUS=$(ls -lt | grep .csv | wc -l)
while [[ true ]]; do
  CURRENT=$(ls -lt | grep .csv | wc -l)
  if [[ $CURRENT < $PREVIOUS ]]; then
    PREVIOUS="$CURRENT"
  fi
  if [[ $CURRENT > $PREVIOUS ]]; then
    FILENAME=$(ls -tr | grep .csv | tail -1)

    RESULT=$(cat "$FILEPATH/$FILENAME" | tail -1 | sed 's/,/ /g' |  awk '{print "sys=" $3 "&dia=" $4 "&hb=" $5 "&afib=" $8}')

    # Open Google Chrome
    google-chrome "$URL?$RESULT"
    PREVIOUS="$CURRENT"
  fi
  # sleep 2
done
